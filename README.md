# The Gateway Server

### Run

1. Clone this repo
2. Clone the following repos in the same level as the gateway repo
   - https://gitlab.com/adred/ds-user-service
   - https://gitlab.com/adred/ds-product-service
   - https://gitlab.com/adred/ds-review-service
3. `cd ds-gateway-server`
4. `docker-compose up`
5. Go to http://localhost:4000/playground
6. Attach the authorization in the header like so

```
{
	"Authorization": "Bearer <token_from_auth0_by_harv>"
}
```

### Queries

Get top products

```javascript
query {
	topProducts {
		name
		price
		inStock
		reviews {
			body
			author {
				id
				username
			}
		}
	}
}
```

Get user

```javascript
query {
	me {
		id
		username
	}
}
```

Get reviews

```javascript
	query {
		reviews {
			body
			product {
				upc
				name
			}
			author {
				id
				username
			}
		}
	}
```
